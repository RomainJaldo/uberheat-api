<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ResultRepository::class)
 */
class Result
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Project::class, inversedBy="configuration", cascade={"persist", "remove"})
     */
    private $project;

    /**
     * @ORM\OneToOne(targetEntity=ProductConfiguration::class, inversedBy="result", cascade={"persist", "remove"})
     */
    private $configuration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getConfiguration(): ?ProductConfiguration
    {
        return $this->configuration;
    }

    public function setConfiguration(?ProductConfiguration $configuration): self
    {
        $this->configuration = $configuration;

        return $this;
    }
}
