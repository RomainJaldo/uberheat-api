<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $basePrice;

    /**
     * @ORM\OneToMany(targetEntity=ProductConfiguration::class, mappedBy="product")
     */
    private $productConfigurations;

    public function __construct()
    {
        $this->depth = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBasePrice(): ?int
    {
        return $this->basePrice;
    }

    public function setBasePrice(?int $basePrice): self
    {
        $this->basePrice = $basePrice;

        return $this;
    }

    /**
     * @return Collection|ProductConfiguration[]
     */
    public function getDepth(): Collection
    {
        return $this->depth;
    }

    public function addDepth(ProductConfiguration $productConfigurations): self
    {
        if (!$this->depth->contains($productConfigurations)) {
            $this->depth[] = $productConfigurations;
            $productConfigurations->setProduct($this);
        }

        return $this;
    }

    public function removeDepth(ProductConfiguration $productConfigurations): self
    {
        if ($this->depth->removeElement($productConfigurations)) {
            // set the owning side to null (unless already changed)
            if ($productConfigurations->getProduct() === $this) {
                $productConfigurations->setProduct(null);
            }
        }

        return $this;
    }
}
