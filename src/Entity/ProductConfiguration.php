<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProductConfigurationRepository::class)
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"rectProductConfiguration" = "RectProductConfiguration", "circProductConfiguration" = "CircProductConfiguration"})
 */
abstract class ProductConfiguration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="depth")
     */
    private $product;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $dB1;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $dB2;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $dB5;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $dB10;

    /**
     * @ORM\OneToOne(targetEntity=Result::class, mappedBy="configuration", cascade={"persist", "remove"})
     */
    private $result;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getDB1(): ?float
    {
        return $this->dB1;
    }

    public function setDB1(?float $dB1): self
    {
        $this->dB1 = $dB1;

        return $this;
    }

    public function getDB2(): ?float
    {
        return $this->dB2;
    }

    public function setDB2(?float $dB2): self
    {
        $this->dB2 = $dB2;

        return $this;
    }

    public function getDB5(): ?float
    {
        return $this->dB5;
    }

    public function setDB5(?float $dB5): self
    {
        $this->dB5 = $dB5;

        return $this;
    }

    public function getDB10(): ?float
    {
        return $this->dB10;
    }

    public function setDB10(?float $dB10): self
    {
        $this->dB10 = $dB10;

        return $this;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setResult(?Result $result): self
    {
        // unset the owning side of the relation if necessary
        if ($result === null && $this->result !== null) {
            $this->result->setConfiguration(null);
        }

        // set the owning side of the relation if necessary
        if ($result !== null && $result->getConfiguration() !== $this) {
            $result->setConfiguration($this);
        }

        $this->result = $result;

        return $this;
    }

    public abstract function getSurface();

}
