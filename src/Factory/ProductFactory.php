<?php

namespace App\Factory;

use App\Entity\Product;
use App\Entity\ProductConfiguration;

class ProductFactory
{
    public function createDefault(array $data, ?ProductConfiguration $configuration): Product
    {
        $product = new Product();
        $product->setName($data[1]);

        if ($configuration !== null) {
            $configuration->setProduct($product);
        }

        return $product;
    }
}
