<?php

namespace App\Factory;

use App\Entity\CircProductConfiguration;
use App\Entity\Product;
use App\Entity\ProductConfiguration;
use App\Entity\RectProductConfiguration;

class ProductConfigurationFactory
{
    public function createFromType(array $data): ProductConfiguration
    {
        if ($data[6] === 'Rectangulaire') {
            $configuration = new RectProductConfiguration();
            $configuration->setWidth((int) $data[2]);
            $configuration->setHeight((int) $data[3]);
            $configuration->setThickness((int) $data[4]);
            $configuration->setDB10((float) $data[7]);
            $configuration->setDB5((float) $data[8]);
            $configuration->setDB2((float) $data[9]);
            $configuration->setDB1((float) $data[10]);
        } else {
            $configuration = new CircProductConfiguration();
            $configuration->setDiameter((int) $data[6]);
            $configuration->setDB10((float) $data[7]);
            $configuration->setDB5((float) $data[8]);
            $configuration->setDB2((float) $data[9]);
            $configuration->setDB1((float) $data[10]);
        }

        return $configuration;
    }
}