<?php

namespace App\Controller;

use App\Entity\Product;
use App\Factory\ProductConfigurationFactory;
use App\Factory\ProductFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    private $em;

    private ProductFactory $productFactory;

    private ProductConfigurationFactory $configurationFactory;

    public function __construct(
        EntityManagerInterface $em,
        ProductFactory $productFactory,
        ProductConfigurationFactory $configurationFactory
    ) {
        $this->em = $em;
        $this->productFactory = $productFactory;
        $this->configurationFactory = $configurationFactory;
    }

    /** @Route("/app", name="app") */
    public function index(): Response
    {
        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
        ]);
    }

    /** @Route("/import-csv", name="import_csv") */
    public function importCSV(Request $request): Response
    {

        /** @var UploadedFile $file */
        $file = $request->files->get('file');

        if($file === null) {
            throw new \Exception('File not found.');
        }

        if($file->getClientOriginalExtension() !== "csv" && $file->getClientMimeType() === "text/csv") {
            throw new \Exception('Extension file invalid.');
        }

        $opendFile = $file->openFile();
        $opendFile->setFlags(\SplFileObject::READ_CSV);
        $opendFile->setCsvControl(";");

        foreach ($opendFile as $line) {
            // REFACTO: Créer une classe Shape
            if($line[0] !== "Rectangulaire" && $line[0] !== "Circulaire") {
                continue;
            }

            $configuration = $this->configurationFactory->createFromType($line);
            $product = $this->productFactory->createDefault($line, $configuration);
            $this->em->persist($configuration);
            $this->em->persist($product);
        }
        $this->em->flush();

        $this->addFlash('success', 'Le fichier a bien été importé');

        return $this->redirectToRoute('app');
    }
}
